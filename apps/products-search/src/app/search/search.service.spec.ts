import { TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

import { SearchService } from './search.service';
import { ProductsEntity } from '../products/+state/products.models';

describe('SearchService', () => {
  let service: SearchService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: []
    });
    service = TestBed.inject(SearchService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('get()', () => {

    const dummyResult: ProductsEntity[] = [
      {
        id: 1,
        brand: 'brand 1',
        description: 'desc 1',
        price: 1234,
        image: 'http://images.com/img.jpg'
      }
    ];
    it('Calls http.get with params', () => {
      service.get('wewe').subscribe( result => {
        expect(result.length).toBe(1);
        expect(result).toEqual(dummyResult);
      });
      const request = httpMock.expectOne('/bff/search/?q=wewe');
      expect(request.request.method).toBe('GET');
      request.flush(dummyResult);
      httpMock.verify();
    });
  });
});
