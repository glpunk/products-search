import { Action, createReducer, on } from '@ngrx/store';
import * as SearchActions from './search.actions';

export const searchFeatureKey = 'search';

export interface State {
  query:string;
}

export const initialState: State = {
  query: ''
};


export const reducer = createReducer(
  initialState,

  on(SearchActions.typeSearch, state => state),

);

