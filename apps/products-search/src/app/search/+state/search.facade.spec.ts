import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';

import { NxModule } from '@nrwl/angular';

import {
  searchFeatureKey,
  State,
  initialState,
  reducer,
} from './search.reducer';
import { SearchFacade } from './search.facade';

interface TestSchema {
  products: State;
}

describe('SearchFacade', () => {
  let facade: SearchFacade;
  let store: Store<TestSchema>;

  beforeEach(() => {});

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [
          StoreModule.forFeature(searchFeatureKey, reducer),
          EffectsModule.forFeature([]),
        ],
        providers: [SearchFacade],
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule,
        ],
      })
      class RootModule {}
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.inject(Store);
      facade = TestBed.inject(SearchFacade);
    });


    describe('typeSearch', () => {
      it('dispatches typeSearch Action', () => {
        const spy = spyOn(store, 'dispatch');
        facade.typeSearch('wewe');
        expect(spy).toHaveBeenCalledWith({"query": "wewe", "type": "[Search] type search action"});
      });
    });

  });
});
