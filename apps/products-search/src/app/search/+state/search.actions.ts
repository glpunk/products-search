import { createAction, props } from '@ngrx/store';

export const typeSearch = createAction(
  '[Search] type search action',
  props<{ query: string }>()
);




