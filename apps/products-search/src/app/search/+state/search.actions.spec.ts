import * as fromSearch from './search.actions';

describe('typeSearch', () => {
  it('should return an action', () => {
    expect(fromSearch.typeSearch({ query: '141'}).type).toBe('[Search] type search action');
  });
});
