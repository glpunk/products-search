import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as SearchActions from './search.actions';
@Injectable()
export class SearchFacade {

  constructor(private store: Store) { }

  typeSearch(query) {
    this.store.dispatch(SearchActions.typeSearch({query}));
  }
}
