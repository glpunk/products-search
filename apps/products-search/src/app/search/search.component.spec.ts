import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SearchFacade } from './+state/search.facade';

import { SearchComponent } from './search.component';

class SearchFacadeMock {
  typeSearch() {}

}
describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchComponent ],
      providers: [{ provide: SearchFacade, useClass: SearchFacadeMock }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
