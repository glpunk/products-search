import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { fromEvent, of } from 'rxjs';
import { concatMap, debounceTime, distinctUntilChanged, filter, map, mergeMap, switchMap, switchMapTo, tap } from 'rxjs/operators';
import { SearchFacade } from './+state/search.facade';

@Component({
  selector: 'products-search-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements AfterViewInit {
  @ViewChild('searchInput') input: ElementRef;
  constructor(private searchFacade: SearchFacade) { }

  ngAfterViewInit(): void {
    fromEvent<any>(this.input.nativeElement, 'keyup').pipe(
      map(event => event.target.value),
      map(value => value.trim()),
      filter(value => {
        const reg = new RegExp(/[0-9]{1,}|\w{3,}/);
        return reg.test(value);
      }),
      debounceTime(400),
      distinctUntilChanged(),
      tap(console.log),
      switchMap(query => of(this.searchFacade.typeSearch( query )))
    ).subscribe();
  }

}
