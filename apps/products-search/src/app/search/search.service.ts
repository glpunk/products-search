import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '@products-search/api-interfaces';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  get(term: string): Observable<Product[]> {
    const options = term ?
      { params: new HttpParams().set('q', term) } :
      {};

    return this.http.get<Product[]>(environment.bff_search_url, options);
  }
}
