import { ComponentFixture, TestBed } from '@angular/core/testing';
import { type } from 'os';

import { ProductComponent } from './product.component';

const productStub = {
  id: 1,
  brand: 'brand 1',
  description: 'desc 1',
  price: 123123,
  image: 'image.jpg'
};

const productStubWithPromo = {
  ...productStub,
  promotion: {
    type: 'palindrome',
    description: '50%',
    price: 10000
  }
}

describe('ProductComponent', () => {
  let component: ProductComponent;
  let fixture: ComponentFixture<ProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    component.product = productStub;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {
    it('whitPromo value with regular product', () => {
      component.product = productStub;
      fixture.detectChanges();
      expect(component.withPromo).toBe('');
    });
    it('whitPromo value with promoted product', () => {
      component.product = productStubWithPromo;
      fixture.detectChanges();
      expect(component.withPromo).toBe('palindrome promotion');
    });
  });
});
