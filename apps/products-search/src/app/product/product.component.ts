import { Component, Input, OnInit } from '@angular/core';
import { ProductsEntity } from './../products/+state/products.models';

@Component({
  selector: 'products-search-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @Input() product: ProductsEntity;
  withPromo = '';
  ngOnInit(): void {
    this.withPromo = this.product.promotion ?
      this.product.promotion.type + ' promotion' :
      '';
  }
}
