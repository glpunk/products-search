import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductComponent } from '../product/product.component';
import { ProductsFacade } from './+state/products.facade';
import { StoreModule, Store } from '@ngrx/store';
import {
  PRODUCTS_FEATURE_KEY,
  State,
  initialState,
  reducer,
} from './+state/products.reducer';
import { ProductsComponent } from './products.component';
import { EffectsModule } from '@ngrx/effects';
import { ProductsEffects } from './+state/products.effects';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ProductsComponent', () => {
  let component: ProductsComponent;
  let fixture: ComponentFixture<ProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot(reducer),
        EffectsModule.forRoot([ProductsEffects])
      ],
      declarations: [ ProductsComponent, ProductComponent ],
      providers: [ProductsFacade]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
