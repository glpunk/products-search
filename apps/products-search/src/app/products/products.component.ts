import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductsFacade } from './+state/products.facade';
import { ProductsEntity } from './+state/products.models';

@Component({
  selector: 'products-search-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products$: Observable<ProductsEntity[]>;

  constructor(private facade: ProductsFacade) { }

  ngOnInit(): void {
    this.products$ = this.facade.allProducts$;
  }

}
