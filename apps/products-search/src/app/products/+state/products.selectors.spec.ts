import { ProductsEntity } from './products.models';
import { State, productsAdapter, initialState } from './products.reducer';
import * as ProductsSelectors from './products.selectors';

describe('Products Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getProductsId = (it) => it['id'];
  const createProductsEntity = (id: number) =>
  ({
    id,
    brand: 'brand 1',
    description: 'desc 1',
    price: 123123,
    image: 'image.jpg'
  } as ProductsEntity);

  let state;

  beforeEach(() => {
    state = {
      products: productsAdapter.setAll(
        [
          createProductsEntity(1),
          createProductsEntity(2),
          createProductsEntity(3),
        ],
        {
          ...initialState,
          selectedId: 2,
          error: ERROR_MSG,
          loaded: true,
        }
      ),
    };
  });

  describe('Products Selectors', () => {
    it('getAllProducts() should return the list of Products', () => {
      const results = ProductsSelectors.getAllProducts(state);
      const selId = getProductsId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe(2);
    });

    it('getSelected() should return the selected Entity', () => {
      const result = ProductsSelectors.getSelected(state);
      const selId = getProductsId(result);

      expect(selId).toBe(2);
    });

    it("getProductsLoaded() should return the current 'loaded' status", () => {
      const result = ProductsSelectors.getProductsLoaded(state);

      expect(result).toBe(true);
    });

    it("getProductsError() should return the current 'error' state", () => {
      const result = ProductsSelectors.getProductsError(state);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
