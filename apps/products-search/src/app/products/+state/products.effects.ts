import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { fetch } from '@nrwl/angular';
import * as ProductsFeature from './products.reducer';
import * as ProductsActions from './products.actions';
import * as searchActions from './../../search/+state/search.actions';
import { SearchService } from '../../search/search.service';
import { of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';

@Injectable()
export class ProductsEffects {
  init$ = createEffect(() =>
    this.actions$.pipe(
      ofType(searchActions.typeSearch),
      switchMap(action =>
        this.searchService.get(action.query).pipe(
          map(products => ProductsActions.loadProductsSuccess({ products })),
          catchError(error => of(ProductsActions.loadProductsFailure({ error }))),
        ),
      )
    )
  );
  constructor(private actions$: Actions, private searchService: SearchService) { }
}
