import { TestBed, async } from '@angular/core/testing';

import { Observable } from 'rxjs';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { NxModule, DataPersistence } from '@nrwl/angular';
import { hot } from '@nrwl/angular/testing';

import { ProductsEffects } from './products.effects';
import * as ProductsActions from './products.actions';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import * as searchActions from './../../search/+state/search.actions';
import { ProductsEntity } from './products.models';
import { SearchService } from '../../search/search.service';

const product = {
  id: 1,
  brand: 'brand 1',
  description: 'desc 1',
  price: 123123,
  image: 'image.jpg'
} as ProductsEntity;
class searchServiceMock {
  get = (q) => {
    return new Observable(subscriber => {
      subscriber.next([product]);
      subscriber.complete();
    });
  }
}

describe('ProductsEffects', () => {
  let actions: Observable<any>;
  let effects: ProductsEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot(), HttpClientTestingModule],
      providers: [
        ProductsEffects,
        DataPersistence,
        provideMockActions(() => actions),
        provideMockStore(),
        { provide: SearchService, useClass: searchServiceMock }
      ],
    });

    effects = TestBed.inject(ProductsEffects);
  });

  describe('init$', () => {
    it('should work', () => {
      actions = hot('-a-|', { a: searchActions.typeSearch({ query: ''}) });

      const expected = hot('-b-|', {
        b: ProductsActions.loadProductsSuccess({ products: [product] }),
      });

      expect(effects.init$).toBeObservable(expected);
    });
  });
});
