import { PromotionInterface } from "./promotion.interface";
import { Product } from '@products-search/api-interfaces';
import { Injectable } from "@nestjs/common";

@Injectable()
export class PalindromePromotion implements PromotionInterface {
  apply(products: Product[], query: string): Product[] {
    console.log(products);
    if (this.isPalindrome(query) === false) return products;
    return products.map(product => {
      return {
        ...product, promotion: {
          type: 'palindrome',
          description: '50%',
          price: product.price * 0.5
        },
      };
    });

  }
  isPalindrome(str: string) {
    return str === str.split('').reverse().join('');
  }
}
