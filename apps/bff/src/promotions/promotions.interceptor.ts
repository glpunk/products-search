import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Product } from '@products-search/api-interfaces';
import { PalindromePromotion } from './palindrome.promotion';

@Injectable()
export class PromotionsInterceptor implements NestInterceptor<Product[]> {
  constructor(private palindromePromo: PalindromePromotion) { }
  intercept(context: ExecutionContext, next: CallHandler): Observable<Product[]> {
    const query = context.getArgByIndex(0).query;
    console.log('query', query);
    return next
      .handle()
      .pipe(
        map(data => this.palindromePromo.apply(data, query['q'])),
      );
  }
}
