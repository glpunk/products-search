import { PalindromePromotion } from './palindrome.promotion';
import { Product } from '@products-search/api-interfaces';

const promotion = new PalindromePromotion();
const products = <Product[]>[
  {
    id: 1,
    brand: 'brand 1',
    description: 'desc 1',
    price: 100
  },
  {
    id: 2,
    brand: 'brand 2',
    description: 'desc 2',
    price: 200
  },
];
describe('PalindromePromotion', () => {
  describe('isPalindrome', () => {
    it('retuns true', () => {
      expect(promotion.isPalindrome('181')).toBe(true);
    });
    it('retuns false', () => {
      expect(promotion.isPalindrome('18a1')).toBe(false);
    });
  });

  describe('apply()', () => {
    it('returns raw products if query is not palindrome', () => {
      const res = promotion.apply(products, 'asd')
      expect(res).toEqual(products);
    });
    it('returns products with promotion applied', () => {
      const res = promotion.apply(products, '181')
      expect(res[0].promotion.price).toEqual(50);
    });
  });
});
