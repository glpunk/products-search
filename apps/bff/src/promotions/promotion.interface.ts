import { Product } from '@products-search/api-interfaces';

export interface PromotionInterface {
  apply(products: Product[], ...args: any): Product[];
}
