import { Module } from '@nestjs/common';
import { SearchModule } from '../search/search.module';
import { SearchService } from '../search/search.service';
import { PalindromePromotion } from './../promotions/palindrome.promotion';
@Module({
  imports: [SearchModule],
  controllers: [],
  providers: [SearchService, PalindromePromotion],
})
export class AppModule { }
