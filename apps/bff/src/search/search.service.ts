import { Injectable } from '@nestjs/common';
import { ApolloClient, InMemoryCache, gql, HttpLink } from '@apollo/client/core';
import fetcher from 'isomorphic-fetch';
import { Product, ProductsData } from '@products-search/api-interfaces';
import { environment } from './../environments/environment'

@Injectable()
export class SearchService {
  client = new ApolloClient({
    link: new HttpLink({
      uri: environment.api_uri,
      fetchOptions: { fetch: fetcher }
    }),
    cache: new InMemoryCache(),
  });
  async getProducts(id: number, text: string) {
    return await this.client.query<ProductsData>({
      query: gql`
      query {
        products(id: ${id}, brand: "${text}", description: "${text}"){
          id,
          brand,
          description,
          image,
          price
        }
      }
      `
    });
  }
}

