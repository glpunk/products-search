import { Test, TestingModule } from '@nestjs/testing';
import { PalindromePromotion } from '../promotions/palindrome.promotion';
import { PromotionsInterceptor } from '../promotions/promotions.interceptor';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';

describe('SearchController', () => {
  let controller: SearchController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SearchController],
      providers: [PromotionsInterceptor, PalindromePromotion, SearchService]
    }).compile();

    controller = module.get<SearchController>(SearchController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
