import { Controller, Get, Query, UseInterceptors } from '@nestjs/common';
import { PromotionsInterceptor } from '../promotions/promotions.interceptor';
import { SearchService } from './search.service';

@Controller()
export class SearchController {
  constructor(private searchService: SearchService) { }

  @UseInterceptors(PromotionsInterceptor)
  @Get('search')
  async searchHandler(@Query('q') q) {
    const regex = new RegExp(/^\d+$/);
    const idParam = regex.test(q) ? q : 0;
    const textParam = idParam === 0 ? q : '';
    const products = (await this.searchProducts(idParam, textParam)).data.products;
    return products;
  }
  searchProducts(id: number, text: string) {
    return this.searchService.getProducts(id, text);
  }
}
