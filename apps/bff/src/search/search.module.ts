import { Module } from '@nestjs/common';
import { PalindromePromotion } from '../promotions/palindrome.promotion';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';

@Module({
  controllers: [SearchController],
  providers: [SearchService, PalindromePromotion]
})
export class SearchModule {}
