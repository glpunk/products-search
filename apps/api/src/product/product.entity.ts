import { Column, Entity, ObjectIdColumn, PrimaryColumn } from 'typeorm';

@Entity('products')
export class Product {
  @ObjectIdColumn()
  _id: string;

  @PrimaryColumn()
  id: number;

  @Column()
  brand: string;

  @Column()
  description: string;

  @Column()
  image: string;

  @Column()
  price: number;
}
