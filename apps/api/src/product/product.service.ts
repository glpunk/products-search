import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './product.entity';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product) private productRepository: Repository<Product>
  ) { }

  async getProducts(id: number, brand: string, description: string): Promise<Product[]> {
    return await this.productRepository.find(
      this.whereCondition(id, brand, description)
    );
  }
  whereCondition(id: number, brand: string, description: string) {
    let whereObj = {};
    if (id) return whereObj = { ...whereObj, where: { id } };
    if (brand || description) {
      whereObj = {
        ...whereObj, where: {
          $or: [
            { brand: new RegExp(`.*${brand}.*`) },
            { description: new RegExp(`.*${description}.*`) },
          ]
        }
      }
    }
    return whereObj;
  }
}
