import { Test } from '@nestjs/testing';
import { Product } from './product.entity';
import { ProductResolver } from './product.resolver';
import { ProductService } from './product.service';

const getProductsMock = jest.fn();

class ProductServiceMock {
  async getProducts(a, b, c) {
    return getProductsMock(a, b, c);
  }
}

describe('ProductResolver', () => {
  let productResolver;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        ProductResolver,
        { provide: ProductService, useClass: ProductServiceMock }
      ]
    }).compile();

    productResolver = await module.get<ProductResolver>(ProductResolver);
  });

  it('Test compiles successfully', () => {
    expect(productResolver).toBeInstanceOf(ProductResolver);
  });

  describe('products', () => {
    it('Calls productService.getProducts', () => {
      expect(getProductsMock).not.toHaveBeenCalled();
      productResolver.products(0, 'a', 'b');
      expect(getProductsMock).toHaveBeenCalledWith(0, 'a', 'b');
    });
  });
});
