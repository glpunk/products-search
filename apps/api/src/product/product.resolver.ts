import { Resolver, Query, Args } from "@nestjs/graphql";
import { ProductService } from "./product.service";
import { ProductType } from "./product.type";

@Resolver(of => ProductType)
export class ProductResolver {
  constructor(private productService: ProductService) { }

  @Query(returns => [ProductType])
  products(
    @Args('id') id: number,
    @Args('brand') brand: string,
    @Args('description') description: string,
  ) {
    return this.productService.getProducts(id, brand, description);
  }
}
