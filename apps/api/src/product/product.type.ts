import { Field, ObjectType, ID } from '@nestjs/graphql';

@ObjectType('Product')
export class ProductType {
  @Field(type => ID)
  id: number;

  @Field()
  brand: string;

  @Field()
  description: string;

  @Field()
  image: string;

  @Field()
  price: number;
}
