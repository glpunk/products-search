import { Test } from '@nestjs/testing';
import { Product } from './product.entity';
import { ProductService } from './product.service';

const findMock = jest.fn();
class ProductRepositoryMock {
  async find(obj) {
    return findMock(obj);
  }
}

describe('ProductService', () => {
  let productService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        ProductService,
        { provide: 'ProductRepository', useClass: ProductRepositoryMock }
      ]
    }).compile();

    productService = await module.get<ProductService>(ProductService);
  });

  it('Test compiles successfully', () => {
    expect(productService).toBeInstanceOf(ProductService);
  });
  describe('whereCondition()', () => {
    it('return empty object', () => {
      const res = productService.whereCondition('');
      expect(res).toStrictEqual({});
    });
    it('returns where with $or', () => {
      const res = productService.whereCondition(0, 'test', 'test');
      expect(res.where['$or'][0].brand).toStrictEqual(/.*test.*/);
      expect(res.where['$or'][1].description).toStrictEqual(RegExp(/.*test.*/));
    });
  });
  describe('getProducts()', () => {
    it('Calls find with whereCondition result', async () => {
      expect(findMock).not.toHaveBeenCalled();
      await productService.getProducts(0, '', '');
      expect(findMock).toHaveBeenCalledWith({});
    });
    it('Returns a products', async () => {
      findMock.mockResolvedValue(<Product[]>[{
        id: 1,
        brand: 'pepe',
        description: 'product 1'
      }]);
      const res = await productService.getProducts(1, '', '');
      expect(res[0].description).toBe('product 1');
    });
  });
});
