export const environment = {
  production: false,
  db: {
    host: 'localhost',
    port: 27017,
    username: 'productListUser',
    password: 'productListPassword',
    database: 'promotions',
    authSource: 'admin',
    synchronize: true,
    useUnifiedTopology: true,
    logging: true
  }
};
