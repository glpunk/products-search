import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from '../product/product.entity';
import { ProductModule } from '../product/product.module';
import { environment } from './../environments/environment';

const {
  host,
  port,
  username,
  password,
  database,
  authSource,
  synchronize,
  logging,
  useUnifiedTopology
} = environment.db;
@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: true
    }),
    TypeOrmModule.forRoot({
      type: 'mongodb',
      host,
      port,
      username,
      password,
      database,
      authSource,
      synchronize,
      logging,
      useUnifiedTopology,
      entities: [Product]
    }),
    ProductModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
