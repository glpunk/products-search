import { getSearchInput, getProduct } from '../support/app.po';

describe('products-search app', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.intercept('/bff/search/?q=12', { fixture: 'regular_product' }).as('regularProduct');
    cy.intercept('/bff/search/?q=gpt', { fixture: 'regular_products' }).as('regularProducts');
    cy.intercept('/bff/search/?q=8', { fixture: 'product_id8' }).as('product8');
    cy.intercept('/bff/search/?q=abba', { fixture: 'products_with_palindrome_promo' }).as('ProductsWithPromo');
  });

  describe('products search', () => {
    describe('Products result', () => {
      context('Without promo', () => {
        it('displays regular product by id', () => {
          getSearchInput().type('12');
          cy.wait(['@regularProduct']).then(interception => {
            getProduct(1).within(() => {
              cy.get('h3').should('have.text', 'vfbjgpt');
              cy.get('.promotion-details').should('not.exist');
            });
          });
        });
        it('displays regular products by phrase', () => {
          getSearchInput().type('gpt');
          cy.wait(['@regularProducts']).then(interception => {
            getProduct(1).within(() => {
              cy.get('p').should('have.text', 'iwpazñ ltxsh stubbed');
              cy.get('.promotion-details').should('not.exist');
            });
            getProduct(2).within(() => {
              cy.get('h3').should('have.text', 'vfbjeggvdfv');
              cy.get('.promotion-details').should('not.exist');
            });
          });
        });
      });
      context('With palidrome promo', () => {
        it('displays product with promo by id', () => {
          getSearchInput().type('8');
          cy.wait(['@product8']).then(interception => {
            getProduct(1).within(() => {
              cy.get('h3').should('have.text', 'ooy eqrceli stubbed');
              cy.get('.promotion-details').should('exist');
              cy.get('.promotion-details').within(() => {
                cy.get('i').should('have.text', '50%');
                cy.get('h4:nth-child(1)').should('have.text', 'CLP498,724');
                cy.get('h4:nth-child(3)').should('have.text', 'CLP249,362');
              });
            });
          });
        });
        it('displays products with promo by phrase', () => {
          getSearchInput().type('abba');
          cy.wait(['@ProductsWithPromo']).then(interception => {
            getProduct(1).within(() => {
              cy.get('h3').should('have.text', 'iñmfdpd');
              cy.get('.promotion-details').should('exist');
              cy.get('.promotion-details').within(() => {
                cy.get('i').should('have.text', '50%');
                cy.get('h4:nth-child(1)').should('have.text', 'CLP533,752');
                cy.get('h4:nth-child(3)').should('have.text', 'CLP266,876');
              });
            });
            getProduct(2).within(() => {
              cy.get('p').should('have.text', 'pqln spqwnrgy');
              cy.get('.promotion-details').should('exist');
              cy.get('.promotion-details').within(() => {
                cy.get('i').should('have.text', '50%');
                cy.get('h4:nth-child(1)').should('have.text', 'CLP571,646');
                cy.get('h4:nth-child(3)').should('have.text', 'CLP285,823');
              });
            });
          });
        });
      });
    });
  });
});
