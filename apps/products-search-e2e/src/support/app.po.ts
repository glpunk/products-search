export const getSearchInput = () => cy.get('body > products-search-root > products-search-search > input[type=text]');
export const getProduct = (n: number) => cy.get(
  `body > products-search-root > products-search-products > products-search-product:nth-child(${n}) > div`
  )
