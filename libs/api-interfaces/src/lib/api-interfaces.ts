interface Promotion {
  type: string;
  description: string;
  price: number;
}
export interface Product {
  id: number;
  brand: string;
  description: string;
  image: string;
  price: number;
  promotion?: Promotion;
}
export interface ProductsData {
  products: Product[]
}
