

# ProductsSearch
Aplicación de búsqueda de productos, realizada con monorepo NX. https://nx.dev/

La arquitectura se compone de 4 recursos:
* Front-End: App Angular implementada con programación reactiva utilizando la dependencia NGRX para el manejo de estados.
* BFF: Back-End For Frontend para comunicación directa con la interfaz de usuario desarrollado con NestJS. Incorpora las reglas de negocio y consume el API.
* API: API graphQL para el consumo de la base de datos, desarrollado con NestJS.
* Base de datos MongoDB según requisitos.

Pruebas unitarias con JEST y de integración con Cypress.

## Requsitos
Instalar línea de comando NX para el manejo de los comando de construcción.

Usando npm:
```
npm install -g nx
```

Usando yarn:
```
yarn global add nx
```

## Base de datos
Clonar repositorio de base de datos de prueba en directorio raíz con nombre de carpeta *products-db*.

## Instalación
Clonar repositorio e instalar dependencias:
```
npm install
```
El desarrollo fue hecho y probado con node v12.6.0

## Ejecución de aplicaciones
### Modo Desarollo
Ejecutar en paralelo en distintas consolas, se explora en la dirección http://localhost:4200
#### UI
```
nx serve
```
#### API
```
nx serve api
```
#### BFF
```
nx serve bff
```

#### Base de datos
Seguir instrucciones de repositorio products-db para ejecutar contenedor docker y provisionarlo.

## Despliegue
### 1. Construcción de aplicaciones
Se hace _build_ de las aplicaciones
#### UI
```
nx build --configuration=production
```
#### API
```
nx build api  --configuration=production
```
#### BFF
```
nx build bff  --configuration=production
```

### 2. Construcción de imágenes docker
Construcción de imágenes docker. Ejecutar los comandos en la carpeta raíz del proyecto.

#### UI

```
docker build -f ./deployment/dockerfiles/front.dockerfile -t products-search-ui .
```
#### API
```
docker build -f ./deployment/dockerfiles/api.dockerfile -t products-search-api .
```
#### BFF

Construcción de imagen
```
docker build -f ./deployment/dockerfiles/bff.dockerfile -t products-search-bff .
```
### 3. Docker Stack
Se levanta docker stack (requiere docker swarm activo)
```
docker stack deploy -c ./deployment/dockerfiles/stack.yml products-search-stack
```

Se explora en la dirección http://localhost:8080

### 4. Base de datos
Ejecutar comando exec en proceso de servicio mongo para provisionar, ejemplo:
```
docker exec products-search-stack_mongo.1.XXXXX bash -c './database/import.sh localhost'
```

El nombre de proceso se obtiene con el comando _docker ps_

Debe existir la carpeta de la base de datos para que pueda tomar el Volumen indicado en el stack.


## Prueba automatizadas

### Pruebas unitarias
#### UI

```
nx test
```
#### API

```
nx test api
```
#### BFF

```
nx test bff
```
### Pruebas de integración

```
nx e2e
```

(Se utiliza intercepción de consulta a servicio para retorno de data dummy)
