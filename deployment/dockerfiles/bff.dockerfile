FROM keymetrics/pm2:latest-alpine

# Bundle APP files
COPY dist/apps/bff dist/
COPY package.json .
COPY deployment/dockerfiles/pm2.json .

ENV NODE_ENV production

# Install app dependencies
ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install --production

CMD [ "pm2-runtime", "start", "pm2.json" ]

EXPOSE 4333
